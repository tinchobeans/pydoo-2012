/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.Vistas;
import Modelo.Articulo;
import java.util.LinkedHashMap;

/**
 *
 * @author tincho
 */
public class MaterialCrear extends javax.swing.JFrame {
    
    private LinkedHashMap<Integer, Articulo> mapaArticulos;

    /**
     * Creates new form MaterialCrear
     */
    private MaterialCrear() {
        initComponents();
    }

    public MaterialCrear(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        this();
        this.mapaArticulos = mapaArticulos;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonApunte = new javax.swing.JButton();
        jButtonCD = new javax.swing.JButton();
        jButtonLibro = new javax.swing.JButton();
        jButtonRevista = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Crear nuevo material");
        setResizable(false);

        jButtonApunte.setText("Apunte");
        jButtonApunte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonApunteActionPerformed(evt);
            }
        });

        jButtonCD.setText("CD");
        jButtonCD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCDActionPerformed(evt);
            }
        });

        jButtonLibro.setText("Libro");
        jButtonLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLibroActionPerformed(evt);
            }
        });

        jButtonRevista.setText("Revista");
        jButtonRevista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRevistaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonApunte)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCD)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonLibro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonRevista)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonApunte)
                    .addComponent(jButtonCD)
                    .addComponent(jButtonLibro)
                    .addComponent(jButtonRevista))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonApunteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonApunteActionPerformed
        // TODO add your handling code here:
        
        Vistas.getInstance().articulo.ventanaCrearApunte(mapaArticulos);
        
    }//GEN-LAST:event_jButtonApunteActionPerformed

    private void jButtonCDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCDActionPerformed
        // TODO add your handling code here:
        
        Vistas.getInstance().articulo.ventanaCrearCd(mapaArticulos);
    }//GEN-LAST:event_jButtonCDActionPerformed

    private void jButtonLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLibroActionPerformed
        // TODO add your handling code here:
        
        Vistas.getInstance().articulo.ventanaCrearLibro(mapaArticulos);
    }//GEN-LAST:event_jButtonLibroActionPerformed

    private void jButtonRevistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRevistaActionPerformed
        // TODO add your handling code here:
        
        Vistas.getInstance().articulo.ventanaCrearRevista(mapaArticulos);
        
    }//GEN-LAST:event_jButtonRevistaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MaterialCrear.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MaterialCrear.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MaterialCrear.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MaterialCrear.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MaterialCrear().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonApunte;
    private javax.swing.JButton jButtonCD;
    private javax.swing.JButton jButtonLibro;
    private javax.swing.JButton jButtonRevista;
    // End of variables declaration//GEN-END:variables
}
