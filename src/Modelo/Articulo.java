/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.Serializacion;
import hirondelle.date4j.DateTime;
import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 *
 * @author tincho
 */
public abstract class Articulo extends Serializacion<Integer,Articulo> implements Serializable {

    private Integer idArticulo;
    private String titulo;
    private String tema;
    private DateTime fechaPublicacion;
    private LinkedHashMap<Integer,Ejemplar> ejemplares = new LinkedHashMap<>();
    private static final long serialVersionUID = 1L;

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @return the tema
     */
    public String getTema() {
        return tema;
    }

    /**
     * @return the fechaPublicacion
     */
    public DateTime getFechaPublicacion() {
        return fechaPublicacion;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @param tema the tema to set
     */
    public void setTema(String tema) {
        this.tema = tema;
    }

    /**
     * @param fechaPublicacion the fechaPublicacion to set
     */
    public void setFechaPublicacion(DateTime fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    /**
     * @return the ejemplares
     */
    public LinkedHashMap<Integer, Ejemplar> getEjemplares() {
        return ejemplares;
    }

    /**
     * @param ejemplares the ejemplares to set
     */
    public void setEjemplares(LinkedHashMap<Integer,Ejemplar> ejemplares) {
        this.ejemplares = ejemplares;
    }

    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append("Titulo: ");
        cadena.append(titulo);
        cadena.append("\nTema: ");
        cadena.append(tema);
        cadena.append("\nFecha de publicacion: ");
        cadena.append(fechaPublicacion);
        return cadena.toString();
    }

    /**
     * @return the idArticulo
     */
    public Integer getIdArticulo() {
        return idArticulo;
    }

    /**
     * @param idArticulo the idArticulo to set
     */
    public void setIdArticulo(Integer idArticulo) {
        this.idArticulo = idArticulo;
    }
}
