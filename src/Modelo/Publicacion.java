/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author tincho
 */
public abstract class Publicacion extends Articulo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private String editorial = "";
    /**
     *
     */
    private String edicion = "";

    /**
     * @return the editorial
     */
    public final String getEditorial() {
        return editorial;
    }

    /**
     * @return the edicion
     */
    public final String getEdicion() {
        return edicion;
    }

    /**
     * @param editorial the editorial to set
     */
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    /**
     * @param edicion the edicion to set
     */
    public void setEdicion(String edicion) {
        this.edicion = edicion;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append(super.toString());
        cadena.append("\nEdicion:");
        cadena.append(edicion);
        cadena.append("\nEditorial:");
        cadena.append(editorial);
        return cadena.toString();
    }
    
}
