/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author tincho
 */
public class ArticuloAlquilado implements Serializable {
    private Integer idArticulo;
    private Integer idEjemplar;
    private static final long serialVersionUID = 1L;

    /**
     * @return the idArticulo
     */
    public Integer getIdArticulo() {
        return idArticulo;
    }

    /**
     * @return the idEjemplar
     */
    public Integer getIdEjemplar() {
        return idEjemplar;
    }

    /**
     * @param idArticulo the idArticulo to set
     */
    public void setIdArticulo(Integer idArticulo) {
        this.idArticulo = idArticulo;
    }

    /**
     * @param idEjemplar the idEjemplar to set
     */
    public void setIdEjemplar(Integer idEjemplar) {
        this.idEjemplar = idEjemplar;
    }
    
    private ArticuloAlquilado() {}
    
    public ArticuloAlquilado(Integer idArticulo, Integer idEjemplar) {
        this.idArticulo = idArticulo;
        this.idEjemplar = idEjemplar;
    }
    
}
