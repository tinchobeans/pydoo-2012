/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import hirondelle.date4j.DateTime;
import java.io.Serializable;

/**
 *
 * @author tincho
 */
public class Cd extends Articulo implements Serializable {
    private String tipoContenido; // Software o Multimedia
    private String formato; // CD o DVD
    private static final long serialVersionUID = 1L;
    
    private Cd() {}
    
    public Cd(Integer idArticulo, String titulo, String tema, DateTime fechaPublicacion, String tipoContenido, String formato) {
        this.tipoContenido = tipoContenido;
        this.formato = formato;
        this.setIdArticulo(idArticulo);
        this.setTitulo(titulo);
        this.setTema(tema);
        this.setFechaPublicacion(fechaPublicacion);
    }

    /**
     * @return the tipoContenido
     */
    public String getTipoContenido() {
        return tipoContenido;
    }

    /**
     * @return the formato
     */
    public String getFormato() {
        return formato;
    }

    /**
     * @param tipoContenido the tipoContenido to set
     */
    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    /**
     * @param formato the formato to set
     */
    public void setFormato(String formato) {
        this.formato = formato;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append(super.toString());
        cadena.append("\nFormato:");
        cadena.append(formato);
        cadena.append("\nTipo de contenido:");
        cadena.append(tipoContenido);
        return cadena.toString();
    }
}
