/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import hirondelle.date4j.DateTime;
import java.io.Serializable;

/**
 *
 * @author tincho
 */
public class Apunte extends Articulo implements Serializable {
    private String materia;
    private String carrera;
    private static final long serialVersionUID = 1L;

    /**
     * @return the materia
     */
    public String getMateria() {
        return materia;
    }

    /**
     * @return the carrera
     */
    public String getCarrera() {
        return carrera;
    }

    /**
     * @param materia the materia to set
     */
    public void setMateria(String materia) {
        this.materia = materia;
    }

    /**
     * @param carrera the carrera to set
     */
    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    
    public Apunte(Integer idArticulo, String titulo, String tema, DateTime fechaPublicacion, Integer stock, String materia, String carrera) {
        this.setIdArticulo(idArticulo);
        this.setTitulo(titulo);
        this.setTema(tema);
        this.setFechaPublicacion(fechaPublicacion);
        this.materia = materia;
        this.carrera = carrera;
    }
    
    private Apunte() {
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append(super.toString());
        cadena.append("\nCarrera:");
        cadena.append(carrera);
        cadena.append("\nMateria:");
        cadena.append(materia);
        return cadena.toString();
    }
}
