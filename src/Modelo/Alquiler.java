/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import hirondelle.date4j.DateTime;
import java.io.Serializable;

/**
 *
 * @author tincho
 */
public class Alquiler implements Serializable {

    private Integer id;
    private Integer dni;
    private DateTime fechaRetiro;
    private DateTime fechaDevolucion;
    private static final long serialVersionUID = 1L;

    private Alquiler() {
    }

    public Alquiler(Integer id, Integer dni, DateTime fechaRetiro, DateTime fechaDevolucion) {
        this.id = id;
        this.dni = dni;
        this.fechaRetiro = fechaRetiro;
        this.fechaDevolucion = fechaDevolucion;
    }

    /**
     * @return the dni
     */
    public Integer getDni() {
        return dni;
    }

    /**
     * @return the fechaRetiro
     */
    public DateTime getFechaRetiro() {
        return fechaRetiro;
    }

    /**
     * @return the fechaDevolucion
     */
    public DateTime getFechaDevolucion() {
        return fechaDevolucion;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(Integer dni) {
        this.dni = dni;
    }

    /**
     * @param fechaRetiro the fechaRetiro to set
     */
    public void setFechaRetiro(DateTime fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    /**
     * @param fechaDevolucion the fechaDevolucion to set
     */
    public void setFechaDevolucion(DateTime fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append("\nDNI:");
        cadena.append(dni);
        cadena.append("\nFecha de Retiro:");
        cadena.append(fechaRetiro);
        cadena.append("\nFecha de Devolucion:");
        cadena.append(fechaDevolucion);
        cadena.append("\nFIN ALQUILER");
        return cadena.toString();
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
}
