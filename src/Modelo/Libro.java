/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import hirondelle.date4j.DateTime;
import java.io.Serializable;

/**
 *
 * @author tincho
 */
public class Libro extends Publicacion implements Serializable {
    private String autor;
    private Integer isbn;
    private static final long serialVersionUID = 1L;
    
    private Libro() {}
    
    public Libro (Integer idArticulo, String titulo, String tema, String edicion, String editorial, DateTime fechaPublicacion, String autor, Integer isbn) {
        this.setIdArticulo(idArticulo);
        this.setTitulo(titulo);
        this.setTema(tema);
        this.setEdicion(edicion);
        this.setEditorial(editorial);
        this.setFechaPublicacion(fechaPublicacion);
        this.autor = autor;
        this.isbn = isbn;
    }

    /**
     * @return the autor
     */
    public String getAutor() {
        return autor;
    }

    /**
     * @return the isbn
     */
    public Integer getIsbn() {
        return isbn;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(String autor) {
        this.autor = autor;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append(super.toString());
        cadena.append("\nAutor:");
        cadena.append(autor);
        cadena.append("\nISBN:");
        cadena.append(isbn);
        return cadena.toString();
    }
}
