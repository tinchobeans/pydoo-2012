/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 *
 * @author tincho
 */
public class Ejemplar implements Serializable {
    
    private Integer id;
    private Boolean disponible = true;
    private LinkedHashMap<Integer,Alquiler> alquileres = new LinkedHashMap<>();
    private static final long serialVersionUID = 1L;

    /**
     * @return the disponibilidad
     */
    public Boolean getDisponibilidad() {
        return getDisponible();
    }

    /**
     * @param disponibilidad the disponibilidad to set
     */
    public void setDisponibilidad(Boolean disponible) {
        this.setDisponible(disponible);
    }
    
    private Ejemplar() {}
    
    public Ejemplar(Integer id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the disponible
     */
    public Boolean getDisponible() {
        return disponible;
    }

    /**
     * @return the alquileres
     */
    public LinkedHashMap<Integer, Alquiler> getAlquileres() {
        return alquileres;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param disponible the disponible to set
     */
    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }

    /**
     * @param alquileres the alquileres to set
     */
    public void setAlquileres(LinkedHashMap<Integer,Alquiler> alquileres) {
        this.alquileres = alquileres;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append("\nID:");
        cadena.append(id);
        cadena.append("\nDisponibilidad:");
        cadena.append(disponible);
        return cadena.toString();
    }
    
}
