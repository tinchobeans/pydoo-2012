/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.Serializacion;
import hirondelle.date4j.DateTime;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author tincho
 */
public class Usuario extends Serializacion<Integer,Usuario> implements Serializable {
    private Integer dni;
    private DateTime fechaNacimiento;
    private String apellido;
    private String nombre;
    private ArrayList<ArticuloAlquilado> itemsAlquilados = new ArrayList<>();
    private static final long serialVersionUID = 1L;

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the dni
     */
    public Integer getDni() {
        return dni;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(Integer dni) {
        this.dni = dni;
    }
    
    public void agregarItem() {
        if (this.verificarCantidadItems()) {
        
        }
    
    }
    
    public void eliminarItem() {
        
    }
    
    private Boolean verificarCantidadItems() {
        return true;
    }
    
    // No se puede crear un Usuario sin parametros
    private Usuario() {
    }
    
    public Usuario(String apellido, String nombre, Integer dni, DateTime fechaNacimiento) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the itemsAlquilados
     */
    public ArrayList<ArticuloAlquilado> getItemsAlquilados() {
        return itemsAlquilados;
    }

    /**
     * @param itemsAlquilados the itemsAlquilados to set
     */
    public void setItemsAlquilados(ArrayList<ArticuloAlquilado> itemsAlquilados) {
        this.itemsAlquilados = itemsAlquilados;
    }

    /**
     * @return the fechaNacimiento
     */
    public DateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(DateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append("\nNombre:");
        cadena.append(nombre);
        cadena.append("\nApellido:");
        cadena.append(apellido);
        cadena.append("\nDNI:");
        cadena.append(dni);
        cadena.append("\nFecha de nacimiento:");
        cadena.append(fechaNacimiento);
        cadena.append("\nItems alquilados:");
        cadena.append(itemsAlquilados);
        return cadena.toString();
    }
    
}
