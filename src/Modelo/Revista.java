/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import hirondelle.date4j.DateTime;
import java.io.Serializable;

/**
 *
 * @author tincho
 */
public class Revista extends Publicacion implements Serializable {
    private Integer issn;
    private static final long serialVersionUID = 1L;

    /**
     * @return the issn
     */
    public Integer getIssn() {
        return issn;
    }

    /**
     * @param issn the issn to set
     */
    public void setIssn(Integer issn) {
        this.issn = issn;
    }
    
    private Revista() {}
    
    public Revista(Integer idArticulo, Integer issn, String editorial, String edicion, String titulo, String tema, DateTime fechaPublicacion) {
        this.setIdArticulo(idArticulo);
        this.setEditorial(editorial);
        this.setEdicion(edicion);
        this.setTitulo(titulo);
        this.setTema(tema);
        this.setFechaPublicacion(fechaPublicacion);
        this.issn = issn;
    }
    
    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        cadena.append(super.toString());
        cadena.append("\nISSN:");
        cadena.append(issn);
        return cadena.toString();
    }
    
}
