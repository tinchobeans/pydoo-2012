/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Alquiler;
import Modelo.Articulo;
import Modelo.Ejemplar;
import Modelo.Usuario;
import Vista.AlquilerCrear;
import Vista.AlquilerEliminar;
import Vista.AlquilerLeer;
import Vista.AlquilerModificar;
import hirondelle.date4j.DateTime;
import java.util.LinkedHashMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author tincho
 */
public class AlquilerControlador {

    public void ventanaEliminar(LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField jTextFieldID) {
        String id = jTextFieldID.getText();
        if (Vistas.getInstance().articulo.validarInput(id)) {
            Articulo articulo = mapaArticulos.get(Integer.parseInt(id));
            LinkedHashMap<Integer, Ejemplar> ejemplares = articulo.getEjemplares();
            if (ejemplares.size() > 0) {
                AlquilerEliminar alquilerEliminar = new AlquilerEliminar(ejemplares);
                alquilerEliminar.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "El articulo no tiene ejemplares, por lo tanto no se pueden eliminar alquileres.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "El ID ingresado es incorrecto.");
        }
    }

    public void ventanaLeer(LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField jTextFieldID) {
        String id = jTextFieldID.getText();
        if (Vistas.getInstance().articulo.validarInput(id)) {
            Articulo a = mapaArticulos.get(Integer.parseInt(id));
            LinkedHashMap<Integer, Ejemplar> ejemplares = a.getEjemplares();
            if (ejemplares.size() > 0) {
                AlquilerLeer alquilerLeer = new AlquilerLeer(ejemplares);
                alquilerLeer.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "El articulo no tiene ejemplares, por lo tanto no se pueden leer alquileres.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "El ID ingresado es incorrecto.");
        }
    }

    public void ventanaModificar(LinkedHashMap<Integer, Usuario> mapaUsuarios, LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField jTextFieldID) {
        String id = jTextFieldID.getText();
        if (Vistas.getInstance().articulo.validarInput(id)) {
            Articulo a = mapaArticulos.get(Integer.parseInt(id));
            LinkedHashMap<Integer, Ejemplar> ejemplares = a.getEjemplares();
            if (ejemplares.size() > 0) {
                AlquilerModificar alquilerModificar = new AlquilerModificar(ejemplares, mapaUsuarios);
                alquilerModificar.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "El articulo no tiene ejemplares, por lo tanto no se pueden modificar alquileres.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "El ID ingresado es incorrecto.");
        }
    }

    public void ventanaCrear(LinkedHashMap<Integer, Articulo> mapaArticulos, LinkedHashMap<Integer, Usuario> mapaUsuarios, JTextField jTextFieldID) {
        String id = jTextFieldID.getText();
        if (Vistas.getInstance().articulo.validarInput(id)) {
            Articulo a = mapaArticulos.get(Integer.parseInt(id));
            LinkedHashMap<Integer, Ejemplar> ejemplares = a.getEjemplares();
            if (ejemplares.size() > 0) {
                AlquilerCrear alquilerCrear = new AlquilerCrear(ejemplares, mapaUsuarios);
                alquilerCrear.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "El articulo no tiene ejemplares, por lo tanto no se pueden crear alquileres.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "El ID ingresado es incorrecto.");
        }
    }

    public void eliminarAlquiler(JTable jt, JComboBox jc, LinkedHashMap<Integer, Ejemplar> mapaEjemplares) {
        if (jt.isValid() && jc.isValid()) {
            int[] alquileresSeleccionados = jt.getSelectedRows();
            if (alquileresSeleccionados.length > 0) {
                Integer ejemplarSeleccionado = (Integer) jc.getSelectedItem();
                for (int i : alquileresSeleccionados) {
                    DefaultTableModel temp = (DefaultTableModel) jt.getModel();
                    Integer indice = (Integer) temp.getValueAt(i, 0);
                    Ejemplar e = mapaEjemplares.get(ejemplarSeleccionado);
                    LinkedHashMap<Integer, Alquiler> alquileres = e.getAlquileres();
                    alquileres.remove(indice);
                }
                actualizarAlquileres(mapaEjemplares, jc, jt);
                JOptionPane.showMessageDialog(null, "Alquileres eliminados con exito.");
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione alquileres a eliminar.");
            }
        }
    }

    private void limpiarAlquileres(JTable jTableAlquileres) {
        if (jTableAlquileres.isValid()) {
            try {
                DefaultTableModel temp = (DefaultTableModel) jTableAlquileres.getModel();
                for (; temp.getRowCount() > 0;) {
                    temp.removeRow(temp.getRowCount() - 1);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(jTableAlquileres, "No existen alquileres registrados en este ejemplar.");
            }
        }
    }

    public void leerAlquileres(LinkedHashMap<Integer, Alquiler> mapaAlquileres, JTable jTableAlquileres) {
        if (jTableAlquileres.isValid()) {
            limpiarAlquileres(jTableAlquileres);
            try {
                for (Integer i : mapaAlquileres.keySet()) {
                    Alquiler a = mapaAlquileres.get(i);
                    String[] array = a.getFechaRetiro().toString().split("-");
                    String fechaRetiro = array[2] + "/" + array[1] + "/" + array[0];
                    array = a.getFechaDevolucion().toString().split("-");
                    String fechaDevolucion = array[2] + "/" + array[1] + "/" + array[0];
                    Object elemento[] = {a.getId(), a.getDni(), fechaRetiro, fechaDevolucion};
                    DefaultTableModel temp = (DefaultTableModel) jTableAlquileres.getModel();
                    temp.addRow(elemento);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(jTableAlquileres, "No existen alquileres registrados en este ejemplar.");
            }
        }
    }

    public void actualizarAlquileres(LinkedHashMap<Integer, Ejemplar> mapaEjemplares, JComboBox jComboBoxEjemplar, JTable jTableAlquileres) {
        if (jComboBoxEjemplar.isValid()) {
            Integer indice = (Integer) jComboBoxEjemplar.getSelectedItem();
            Ejemplar e = mapaEjemplares.get(indice);
            LinkedHashMap<Integer, Alquiler> mapaAlquileres = e.getAlquileres();
            leerAlquileres(mapaAlquileres, jTableAlquileres);
        }
    }

    public void leerEjemplares(LinkedHashMap<Integer, Ejemplar> mapaEjemplares, JComboBox jComboBoxEjemplar) {
        try {
            for (Integer i : mapaEjemplares.keySet()) {
                Ejemplar e = mapaEjemplares.get(i);
                DefaultComboBoxModel temp = (DefaultComboBoxModel) jComboBoxEjemplar.getModel();
                temp.addElement(e.getId());
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "No existen ejemplares registrados en este articulo.");
        }
    }

    public void agregarAlquiler(JTextField id, JTextField dni, JTextField retiroDia, JTextField retiroMes, JTextField retiroY, JTextField devolucionDia, JTextField devolucionMes, JTextField devolucionY, JComboBox jc, LinkedHashMap<Integer, Ejemplar> mapaEjemplares, LinkedHashMap<Integer, Usuario> mapaUsuarios) {
        if (jc.isValid() && dni.isValid() && retiroDia.isValid() && retiroMes.isValid() && retiroY.isValid() && devolucionDia.isValid() && devolucionMes.isValid() && devolucionY.isValid()) {
            Boolean idValido = Vistas.getInstance().articulo.validarInput(id.getText());
            Boolean noRepetido = false;
            Boolean dniValido = Vistas.getInstance().articulo.validarInput(dni.getText());
            Boolean dniExiste = false;
            if (dniValido) {
                dniExiste = mapaUsuarios.containsKey(Integer.parseInt(dni.getText()));
            }
            if (idValido) {
                Integer indice = (Integer) jc.getSelectedItem();
                Ejemplar e = mapaEjemplares.get(indice);
                LinkedHashMap<Integer, Alquiler> alquileres = e.getAlquileres();
                noRepetido = !alquileres.containsKey(Integer.parseInt(id.getText()));
            }
            StringBuilder fecha = new StringBuilder();
            fecha.append(retiroY.getText());
            fecha.append("-");
            fecha.append(retiroMes.getText());
            fecha.append("-");
            fecha.append(retiroDia.getText());
            JTextField retiro = new JTextField(fecha.toString());
            fecha = new StringBuilder();
            fecha.append(devolucionY.getText());
            fecha.append("-");
            fecha.append(devolucionMes.getText());
            fecha.append("-");
            fecha.append(devolucionDia.getText());
            JTextField devolucion = new JTextField(fecha.toString());
            Boolean devolucionValido = validarFecha(devolucion) && devolucion.getText().compareTo(retiro.getText()) >= 0;
            Boolean retiroValido = validarFecha(retiro) && retiro.getText().compareTo(devolucion.getText()) <= 0;
            if (idValido && dniValido && retiroValido && devolucionValido && noRepetido && dniExiste) {
                Integer indice = (Integer) jc.getSelectedItem();
                Ejemplar e = mapaEjemplares.get(indice);
                LinkedHashMap<Integer, Alquiler> alquileres = e.getAlquileres();
                if (!alquileres.containsKey(Integer.parseInt(id.getText()))) {
                    Integer numeroID = Integer.parseInt(id.getText());
                    Integer numeroDNI = Integer.parseInt(dni.getText());
                    DateTime fechaRetiro = new DateTime(retiro.getText());
                    DateTime fechaDevolucion = new DateTime(devolucion.getText());
                    Alquiler a = new Alquiler(numeroID, numeroDNI, fechaRetiro, fechaDevolucion);
                    alquileres.put(numeroID, a);
                    JOptionPane.showMessageDialog(null, "El alquiler ha sido creado con exito.");
                } else {
                    JOptionPane.showMessageDialog(null, "El ID del alquiler ya existe, ingrese otro ID.");
                }
            } else {
                StringBuilder st = new StringBuilder();
                st.append("Los datos del alquiler son invalidos:");
                if (!idValido) {
                    st.append("\nID invalido: ingrese un numero entero positivo.");
                } else {
                    if (!noRepetido) {
                        st.append("\nID ya existe: ingrese otro ID.");
                    }
                }
                if (!dniValido) {
                    st.append("\nDNI invalido: ingrese un numero entero positivo.");
                }
                if (!dniExiste) {
                    st.append("\nEl DNI no existe en el sistema.");
                }
                if (!retiroValido) {
                    st.append("\nFecha de retiro invalida, formato: DD/MM/AAAA");
                }
                if (!devolucionValido) {
                    st.append("\nFecha de devolucion invalida, formato: DD/MM/AAAA");
                }
                JOptionPane.showMessageDialog(null, st.toString());
            }
        }
    }

    public Boolean validarFecha(JTextField retiro) {
        Boolean resultado;
        try {
            if (retiro.getText().isEmpty()) {
                resultado = false;
            } else {
                String[] valores = retiro.getText().split("-");
                Integer y = Integer.parseInt(valores[0]);
                Integer mm = Integer.parseInt(valores[1]);
                Integer dd = Integer.parseInt(valores[2]);
                DateTime d = new DateTime(y, mm, dd, 0, 0, 0, 0);
                resultado = true;
            }
        } catch (Exception e) {
            resultado = false;
        }
        return resultado;
    }

    public void seleccionarAlquiler(JTable jTableAlquileres, JComboBox jComboBoxEjemplar, JTextField jTextFieldEjemplar, JTextField jTextFieldID, JTextField jTextFieldDNI, JTextField retiroDia, JTextField retiroMes, JTextField retiroY, JTextField devolucionDia, JTextField devolucionMes, JTextField devolucionY) {
        if (jTableAlquileres.isValid() && jComboBoxEjemplar.isValid() && jTextFieldDNI.isValid() && retiroDia.isValid() && retiroMes.isValid() && retiroY.isValid() && devolucionDia.isValid() && devolucionMes.isValid() && devolucionY.isValid()) {
            if (jTableAlquileres.getSelectedRow() != -1) {
                Integer fila = (Integer) jTableAlquileres.getSelectedRow();
                String id = jTableAlquileres.getValueAt(fila, 0).toString();
                String dni = jTableAlquileres.getValueAt(fila, 1).toString();
                String[] retiro = jTableAlquileres.getValueAt(fila, 2).toString().split("/");
                String[] devolucion = jTableAlquileres.getValueAt(fila, 3).toString().split("/");
                String ejemplar = jComboBoxEjemplar.getSelectedItem().toString();
                jTextFieldEjemplar.setText(ejemplar);
                jTextFieldID.setText(id);
                jTextFieldDNI.setText(dni);
                retiroY.setText(retiro[2]);
                retiroMes.setText(retiro[1]);
                retiroDia.setText(retiro[0]);
                devolucionY.setText(devolucion[2]);
                devolucionMes.setText(devolucion[1]);
                devolucionDia.setText(devolucion[0]);
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un alquiler.");
            }
        }
    }

    public void editarAlquiler(LinkedHashMap<Integer, Usuario> mapaUsuarios, LinkedHashMap<Integer, Ejemplar> mapaEjemplares, JTextField jTextFieldEjemplar, JTextField jTextFieldID, JTextField jTextFieldDNI, JTextField retiroDia, JTextField retiroMes, JTextField retiroY, JTextField devolucionDia, JTextField devolucionMes, JTextField devolucionY, JComboBox cb, JTable jt) {
        if (jTextFieldEjemplar.isValid() && jTextFieldDNI.isValid() && retiroDia.isValid() && retiroMes.isValid() && retiroY.isValid() && devolucionDia.isValid() && devolucionMes.isValid() && devolucionY.isValid() && cb.isValid() && jt.isValid()) {
            Boolean id = Vistas.getInstance().articulo.validarInput(jTextFieldID.getText());
            Boolean dni = Vistas.getInstance().articulo.validarInput(jTextFieldDNI.getText());
            Boolean existe = false;
            if (dni) {
                existe = mapaUsuarios.containsKey(Integer.parseInt(jTextFieldDNI.getText()));
            }
            StringBuilder fecha = new StringBuilder();
            fecha.append(retiroY.getText());
            fecha.append("-");
            fecha.append(retiroMes.getText());
            fecha.append("-");
            fecha.append(retiroDia.getText());
            JTextField jTextFieldFechaRetiro = new JTextField(fecha.toString());
            fecha = new StringBuilder();
            fecha.append(devolucionY.getText());
            fecha.append("-");
            fecha.append(devolucionMes.getText());
            fecha.append("-");
            fecha.append(devolucionDia.getText());
            JTextField jTextFieldFechaDevolucion = new JTextField(fecha.toString());
            Boolean retiro = validarFecha(jTextFieldFechaRetiro) && jTextFieldFechaRetiro.getText().compareTo(jTextFieldFechaDevolucion.getText()) <= 0;
            Boolean devolucion = validarFecha(jTextFieldFechaDevolucion) && jTextFieldFechaDevolucion.getText().compareTo(jTextFieldFechaRetiro.getText()) >= 0;
            if (id && dni && retiro && devolucion && existe) {
                Integer ejemplar = Integer.parseInt(jTextFieldEjemplar.getText());
                Ejemplar e = mapaEjemplares.get(ejemplar);
                LinkedHashMap<Integer, Alquiler> alquileres = e.getAlquileres();
                Integer numeroID = Integer.parseInt(jTextFieldID.getText());
                Alquiler a = alquileres.get(numeroID);
                Integer numeroDNI = Integer.parseInt(jTextFieldDNI.getText());
                a.setDni(numeroDNI);
                a.setFechaRetiro(new DateTime(jTextFieldFechaRetiro.getText()));
                a.setFechaDevolucion(new DateTime(jTextFieldFechaDevolucion.getText()));
                actualizarAlquileres(mapaEjemplares, cb, jt);
                JOptionPane.showMessageDialog(null, "Alquiler modificado con exito.");
            } else {
                StringBuilder st = new StringBuilder();
                st.append("Datos invalidos:");
                if (!id) {
                    st.append("\nID invalido");
                }
                if (!dni) {
                    st.append("\nDNI invalido");
                }
                if (!existe) {
                    st.append("\nEl DNI no existe en el sistema.");
                }
                if (!retiro) {
                    st.append("\nFecha de retiro invalida");
                }
                if (!devolucion) {
                    st.append("\nFecha de devolucion invalida");
                }
                JOptionPane.showMessageDialog(null, st.toString());
            }
        }
    }
}
