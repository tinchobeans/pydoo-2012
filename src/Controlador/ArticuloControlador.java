/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Apunte;
import Modelo.Articulo;
import Modelo.Cd;
import Modelo.Ejemplar;
import Modelo.Libro;
import Modelo.Publicacion;
import Modelo.Revista;
import Vista.MaterialCrear;
import Vista.MaterialCrearApunte;
import Vista.MaterialCrearCD;
import Vista.MaterialCrearLibro;
import Vista.MaterialCrearRevista;
import Vista.MaterialEliminar;
import Vista.MaterialLeer;
import Vista.MaterialLeerApunte;
import Vista.MaterialLeerCD;
import Vista.MaterialLeerLibro;
import Vista.MaterialLeerRevista;
import Vista.MaterialMain;
import Vista.MaterialModificar;
import Vista.MaterialModificarApunte;
import Vista.MaterialModificarCD;
import Vista.MaterialModificarLibro;
import Vista.MaterialModificarRevista;
import hirondelle.date4j.DateTime;
import java.util.LinkedHashMap;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author tincho
 */
public class ArticuloControlador {

    public void buscarArticulo(JTextField id, JTextArea informacionArticulo, LinkedHashMap<Integer, Articulo> articulos) {
        if (validarInput(id.getText())) {
            Integer key = Integer.parseInt(id.getText());
            if (articulos.containsKey(key)) {
                informacionArticulo.setText(articulos.get(key).toString());

            } else {
                JOptionPane.showMessageDialog(null, "El ID del articulo no existe.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "El ID del articulo es invalido.");
        }
    }

    public Boolean validarInput(String id) {
        try {
            return !id.isEmpty() && (Integer.parseInt(id)) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void ventanaLeer(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        if (mapaArticulos.size() > 0) {
            MaterialLeer materialLeer = new MaterialLeer(mapaArticulos);
            materialLeer.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "No se pueden leer articulos porque no existen articulos.");
        }
    }

    public void leerArticulos(LinkedHashMap<Integer, Articulo> mapaArticulos, JTable jTableArticulos) {
        if (jTableArticulos.isValid()) {
            limpiarArticulos(jTableArticulos);
            DefaultTableModel temp = (DefaultTableModel) jTableArticulos.getModel();
            for (Integer i : mapaArticulos.keySet()) {
                Articulo a = mapaArticulos.get(i);
                String[] fecha = a.getFechaPublicacion().toString().split("-");
                StringBuilder st = new StringBuilder();
                st.append(fecha[2]);
                st.append("/");
                st.append(fecha[1]);
                st.append("/");
                st.append(fecha[0]);
                Object[] elemento = {a.getIdArticulo(), a.getTitulo(), a.getTema(), st.toString()};
                temp.addRow(elemento);
            }
        }
    }

    private void limpiarArticulos(JTable jTableArticulos) {
        DefaultTableModel temp = (DefaultTableModel) jTableArticulos.getModel();
        for (; temp.getRowCount() > 0;) {
            temp.removeRow(0);
        }
    }

    public void ventanaMain(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialMain materialMain = new MaterialMain(mapaArticulos);
        materialMain.setVisible(true);
    }

    public void ventanaMasInformacion(LinkedHashMap<Integer, Articulo> mapaArticulos, JTable jTableArticulos) {
        int indice = jTableArticulos.getSelectedRow();
        if (indice > -1) {
            Integer i = Integer.parseInt(jTableArticulos.getValueAt(indice, 0).toString());
            Articulo a = mapaArticulos.get(i);
            try {
                MaterialLeerApunte materialApunte = new MaterialLeerApunte((Apunte) a);
                materialApunte.setVisible(true);
            } catch (Exception e) {
            }
            try {
                MaterialLeerCD materialCd = new MaterialLeerCD((Cd) a);
                materialCd.setVisible(true);
            } catch (Exception e) {
            }
            try {
                MaterialLeerRevista materialRevista = new MaterialLeerRevista((Revista) a);
                materialRevista.setVisible(true);
            } catch (Exception e) {
            }
            try {
                MaterialLeerLibro materialLibro = new MaterialLeerLibro((Libro) a);
                materialLibro.setVisible(true);
            } catch (Exception exc) {
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un articulo.");
        }
    }

    public void ventanaModificarMain(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        if (mapaArticulos.size() > 0) {
            MaterialModificar materialModificar = new MaterialModificar(mapaArticulos);
            materialModificar.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "No pueden modificarse articulos porque no existen articulos cargados.");
        }
    }

    public void ventanaModificar(LinkedHashMap<Integer, Articulo> mapaArticulos, JTable jTableArticulos) {
        if (jTableArticulos.isValid()) {
            int indice = jTableArticulos.getSelectedRow();
            if (indice > -1) {
                Integer i = Integer.parseInt(jTableArticulos.getValueAt(indice, 0).toString());
                Articulo a = mapaArticulos.get(i);
                try {
                    MaterialModificarApunte materialApunte = new MaterialModificarApunte((Apunte) a);
                    materialApunte.setVisible(true);
                } catch (Exception e) {
                }
                try {
                    MaterialModificarCD materialCd = new MaterialModificarCD((Cd) a);
                    materialCd.setVisible(true);
                } catch (Exception e) {
                }
                try {
                    MaterialModificarRevista materialRevista = new MaterialModificarRevista((Revista) a);
                    materialRevista.setVisible(true);
                } catch (Exception e) {
                }
                try {
                    MaterialModificarLibro materialLibro = new MaterialModificarLibro((Libro) a);
                    materialLibro.setVisible(true);
                } catch (Exception exc) {
                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un articulo.");
            }
        }
    }

    private void leerArticulo(Articulo a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year) {
        id.setText(a.getIdArticulo().toString());
        titulo.setText(a.getTitulo());
        tema.setText(a.getTema());
        String[] fecha = a.getFechaPublicacion().toString().split("-");
        dia.setText(fecha[2]);
        mes.setText(fecha[1]);
        year.setText(fecha[0]);
    }

    private void leerPublicacion(Publicacion a, JTextField editorial, JTextField edicion) {
        editorial.setText(a.getEditorial());
        edicion.setText(a.getEdicion());
    }

    public void leerApunte(Apunte a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField materia, JTextField carrera) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && materia.isValid() && carrera.isValid()) {
            leerArticulo(a, id, titulo, tema, dia, mes, year);
            materia.setText(a.getMateria());
            carrera.setText(a.getCarrera());
        }
    }

    public void leerCD(Cd a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField tipoContenido, JTextField formato) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && tipoContenido.isValid() && formato.isValid()) {
            leerArticulo(a, id, titulo, tema, dia, mes, year);
            tipoContenido.setText(a.getTipoContenido());
            formato.setText(a.getFormato());
        }
    }

    public void leerLibro(Libro a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField editorial, JTextField edicion, JTextField autor, JTextField issbn) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && editorial.isValid() && edicion.isValid() && autor.isValid() && issbn.isValid()) {
            leerArticulo(a, id, titulo, tema, dia, mes, year);
            leerPublicacion(a, editorial, edicion);
            autor.setText(a.getAutor());
            issbn.setText(a.getIsbn().toString());
        }
    }

    public void leerRevista(Revista a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField editorial, JTextField edicion, JTextField issn) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && editorial.isValid() && edicion.isValid() && issn.isValid()) {
            leerArticulo(a, id, titulo, tema, dia, mes, year);
            leerPublicacion(a, editorial, edicion);
            issn.setText(a.getIssn().toString());
        }

    }

    private void modificarArticulo(Articulo a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year) {
        StringBuilder fecha = new StringBuilder();
        fecha.append(year.getText());
        fecha.append("-");
        fecha.append(mes.getText());
        fecha.append("-");
        fecha.append(dia.getText());
        Boolean fechaValida = Vistas.getInstance().alquiler.validarFecha(new JTextField(fecha.toString()));
        if (fechaValida) {
            a.setIdArticulo(Integer.parseInt(id.getText()));
            a.setTitulo(titulo.getText());
            a.setTema(tema.getText());
            a.setFechaPublicacion(new DateTime(fecha.toString()));
        } else {
            JOptionPane.showMessageDialog(null, "Fecha invalida, formato: DD/MM/AAAA");
        }

    }

    private void modificarPublicacion(Publicacion a, JTextField editorial, JTextField edicion) {
        a.setEdicion(edicion.getText());
        a.setEditorial(editorial.getText());
    }

    public void modificarApunte(Apunte a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField materia, JTextField carrera) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && materia.isValid() && carrera.isValid()) {
            modificarArticulo(a, id, titulo, tema, dia, mes, year);
            a.setMateria(materia.getText());
            a.setCarrera(carrera.getText());
            JOptionPane.showMessageDialog(null, "Articulo modificado exitosamente");
        }
    }

    public void modificarCD(Cd a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JComboBox tipoContenido, JComboBox formato) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && tipoContenido.isValid() && formato.isValid()) {
            modificarArticulo(a, id, titulo, tema, dia, mes, year);
            a.setFormato(formato.getSelectedItem().toString());
            a.setTipoContenido(tipoContenido.getSelectedItem().toString());
            JOptionPane.showMessageDialog(null, "Articulo modificado exitosamente");
        }
    }

    public void modificarLibro(Libro a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField editorial, JTextField edicion, JTextField autor, JTextField issbn) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && editorial.isValid() && edicion.isValid() && autor.isValid() && issbn.isValid()) {
            modificarArticulo(a, id, titulo, tema, dia, mes, year);
            modificarPublicacion(a, editorial, edicion);
            a.setAutor(autor.getText());
            a.setIsbn(Integer.parseInt(issbn.getText()));
            JOptionPane.showMessageDialog(null, "Articulo modificado exitosamente");
        }

    }

    public void modificarRevista(Revista a, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField editorial, JTextField edicion, JTextField issn) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && editorial.isValid() && edicion.isValid() && issn.isValid()) {
            modificarArticulo(a, id, titulo, tema, dia, mes, year);
            modificarPublicacion(a, editorial, edicion);
            a.setIssn(Integer.parseInt(issn.getText()));
            JOptionPane.showMessageDialog(null, "Articulo modificado exitosamente");
        }

    }

    public void eliminarArticulo(LinkedHashMap<Integer, Articulo> mapaArticulos, JTable articulos) {
        if (articulos.getSelectedRows().length > 0) {
            for (int i : articulos.getSelectedRows()) {
                DefaultTableModel temp = (DefaultTableModel) articulos.getModel();
                Integer indice = (Integer) temp.getValueAt(i, 0);
                mapaArticulos.remove(indice);
            }
            leerArticulos(mapaArticulos, articulos);
            JOptionPane.showMessageDialog(null, "Articulos eliminados exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione articulos a eliminar");
        }
    }

    public void ventanaEliminarMain(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialEliminar materialEliminar = new MaterialEliminar(mapaArticulos);
        materialEliminar.setVisible(true);
    }

    public void crearApunte(LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField jTextFieldID, JTextField jTextFieldTitulo, JTextField jTextFieldTema, JTextField jTextFieldDia, JTextField jTextFieldMes, JTextField jTextFieldY, JTextField jTextFieldMateria, JTextField jTextFieldCarrera, JTextField ejemplares) {
        if (jTextFieldID.isValid() && jTextFieldTitulo.isValid() && jTextFieldTema.isValid() && jTextFieldDia.isValid() && jTextFieldMes.isValid() && jTextFieldY.isValid() && jTextFieldMateria.isValid() && jTextFieldCarrera.isValid() && ejemplares.isValid()) {
            Boolean idValido = Vistas.getInstance().articulo.validarInput(jTextFieldID.getText());
            Boolean ejemplaresValido = Vistas.getInstance().articulo.validarInput(ejemplares.getText());
            Boolean noRepetido = null;
            if (idValido) {
                noRepetido = !mapaArticulos.containsKey(Integer.parseInt(jTextFieldID.getText()));
            }
            StringBuilder sb = new StringBuilder();
            sb.append(jTextFieldY.getText());
            sb.append("-");
            sb.append(jTextFieldMes.getText());
            sb.append("-");
            sb.append(jTextFieldDia.getText());
            JTextField fecha = new JTextField(sb.toString());
            Boolean fechaValida = Vistas.getInstance().alquiler.validarFecha(fecha);
            if (idValido && noRepetido != null && noRepetido && fechaValida && ejemplaresValido) {
                Apunte a = new Apunte(Integer.parseInt(jTextFieldID.getText()), jTextFieldTitulo.getText(), jTextFieldTema.getText(), new DateTime(fecha.getText()), null, jTextFieldMateria.getText(), jTextFieldCarrera.getText());
                for (int i = 1; i < Integer.parseInt(ejemplares.getText()); i++) {
                    a.getEjemplares().put(i, new Ejemplar(i));
                }
                mapaArticulos.put(a.getIdArticulo(), a);
                JOptionPane.showMessageDialog(null, "Articulo creado exitosamente");
            } else {
                sb = new StringBuilder();
                sb.append("Datos invalidos:");
                if (!idValido) {
                    sb.append("\nID invalido.");
                } else {
                    if (noRepetido != null && !noRepetido) {
                        sb.append("\nID ya existe.");
                    }
                }
                if (!fechaValida) {
                    sb.append("\nFecha invalida, formato: DD/MM/AAAA");
                }
                if (!ejemplaresValido) {
                    sb.append("\nNumero de ejemplares invalido.");
                }
                JOptionPane.showMessageDialog(null, sb.toString());
            }
        }

    }

    public void crearCD(LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JComboBox tipoContenido, JComboBox formato, JTextField ejemplares) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && tipoContenido.isValid() && formato.isValid() && ejemplares.isValid()) {
            Boolean idValido = Vistas.getInstance().articulo.validarInput(id.getText());
            Boolean ejemplaresValido = Vistas.getInstance().articulo.validarInput(ejemplares.getText());
            Boolean noRepetido = null;
            if (idValido) {
                noRepetido = !mapaArticulos.containsKey(Integer.parseInt(id.getText()));
            }
            StringBuilder sb = new StringBuilder();
            sb.append(year.getText());
            sb.append("-");
            sb.append(mes.getText());
            sb.append("-");
            sb.append(dia.getText());
            JTextField fecha = new JTextField(sb.toString());
            Boolean fechaValida = Vistas.getInstance().alquiler.validarFecha(fecha);
            if (idValido && noRepetido != null && noRepetido && fechaValida && ejemplaresValido) {
                Cd a = new Cd(Integer.parseInt(id.getText()), titulo.getText(), tema.getText(), new DateTime(fecha.getText()), tipoContenido.getSelectedItem().toString(), formato.getSelectedItem().toString());
                for (int i = 1; i < Integer.parseInt(ejemplares.getText()); i++) {
                    a.getEjemplares().put(i, new Ejemplar(i));
                }
                mapaArticulos.put(a.getIdArticulo(), a);
                JOptionPane.showMessageDialog(null, "Articulo creado exitosamente");
            } else {
                sb = new StringBuilder();
                sb.append("Datos invalidos:");
                if (!idValido) {
                    sb.append("\nID invalido.");
                } else {
                    if (noRepetido != null && !noRepetido) {
                        sb.append("\nID ya existe.");
                    }
                }
                if (!fechaValida) {
                    sb.append("\nFecha invalida, formato: DD/MM/AAAA");
                }
                if (!ejemplaresValido) {
                    sb.append("\nNumero de ejemplares invalido.");
                }
                JOptionPane.showMessageDialog(null, sb.toString());
            }
        }
    }

    public void crearLibro(LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField editorial, JTextField edicion, JTextField autor, JTextField issbn, JTextField ejemplares) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && editorial.isValid() && edicion.isValid() && autor.isValid() && issbn.isValid() && ejemplares.isValid()) {
            Boolean idValido = Vistas.getInstance().articulo.validarInput(id.getText());
            Boolean issbnValido = Vistas.getInstance().articulo.validarInput(issbn.getText());
            Boolean ejemplaresValido = Vistas.getInstance().articulo.validarInput(ejemplares.getText());
            Boolean noRepetido = null;
            if (idValido) {
                noRepetido = !mapaArticulos.containsKey(Integer.parseInt(id.getText()));
            }
            StringBuilder sb = new StringBuilder();
            sb.append(year.getText());
            sb.append("-");
            sb.append(mes.getText());
            sb.append("-");
            sb.append(dia.getText());
            JTextField fecha = new JTextField(sb.toString());
            Boolean fechaValida = Vistas.getInstance().alquiler.validarFecha(fecha);
            if (issbnValido && idValido && noRepetido != null && noRepetido && fechaValida && ejemplaresValido) {
                Libro a = new Libro(Integer.parseInt(id.getText()), titulo.getText(), tema.getText(), edicion.getText(), editorial.getText(), new DateTime(fecha.getText()), autor.getText(), Integer.parseInt(issbn.getText()));
                for (int i = 1; i < Integer.parseInt(ejemplares.getText()); i++) {
                    a.getEjemplares().put(i, new Ejemplar(i));
                }
                mapaArticulos.put(a.getIdArticulo(), a);
                JOptionPane.showMessageDialog(null, "Articulo creado exitosamente");
            } else {
                sb = new StringBuilder();
                sb.append("Datos invalidos:");
                if (!idValido) {
                    sb.append("\nID invalido.");
                } else {
                    if (noRepetido != null && !noRepetido) {
                        sb.append("\nID ya existe.");
                    }
                }
                if (!fechaValida) {
                    sb.append("\nFecha invalida, formato: DD/MM/AAAA");
                }
                if (!issbnValido) {
                    sb.append("\nISSBN invalido.");
                }
                JOptionPane.showMessageDialog(null, sb.toString());
            }
        }
    }

    public void crearRevista(LinkedHashMap<Integer, Articulo> mapaArticulos, JTextField id, JTextField titulo, JTextField tema, JTextField dia, JTextField mes, JTextField year, JTextField editorial, JTextField edicion, JTextField issn,JTextField ejemplares) {
        if (id.isValid() && titulo.isValid() && tema.isValid() && dia.isValid() && mes.isValid() && year.isValid() && editorial.isValid() && edicion.isValid() && issn.isValid() && ejemplares.isValid()) {
            Boolean idValido = Vistas.getInstance().articulo.validarInput(id.getText());
            Boolean issnValido = Vistas.getInstance().articulo.validarInput(issn.getText());
            Boolean ejemplaresValido = Vistas.getInstance().articulo.validarInput(ejemplares.getText());
            Boolean noRepetido = null;
            if (idValido) {
                noRepetido = !mapaArticulos.containsKey(Integer.parseInt(id.getText()));
            }
            StringBuilder sb = new StringBuilder();
            sb.append(year.getText());
            sb.append("-");
            sb.append(mes.getText());
            sb.append("-");
            sb.append(dia.getText());
            JTextField fecha = new JTextField(sb.toString());
            Boolean fechaValida = Vistas.getInstance().alquiler.validarFecha(fecha);
            if (issnValido && idValido && noRepetido != null && noRepetido && fechaValida && ejemplaresValido) {
                Revista a = new Revista(Integer.parseInt(id.getText()), Integer.parseInt(issn.getText()), editorial.getText(), edicion.getText(), titulo.getText(), tema.getText(), new DateTime(fecha.getText()));
                for (int i = 1; i < Integer.parseInt(ejemplares.getText()); i++) {
                    a.getEjemplares().put(i, new Ejemplar(i));
                }
                mapaArticulos.put(a.getIdArticulo(), a);
                JOptionPane.showMessageDialog(null, "Articulo creado exitosamente");
            } else {
                sb = new StringBuilder();
                sb.append("Datos invalidos:");
                if (!idValido) {
                    sb.append("\nID invalido.");
                } else {
                    if (noRepetido != null && !noRepetido) {
                        sb.append("\nID ya existe.");
                    }
                }
                if (!fechaValida) {
                    sb.append("\nFecha invalida, formato: DD/MM/AAAA");
                }
                if (!issnValido) {
                    sb.append("\nISSN invalido.");
                }
                JOptionPane.showMessageDialog(null, sb.toString());
            }
        }
    }

    public void ventanaCrearMain(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialCrear materialCrear = new MaterialCrear(mapaArticulos);
        materialCrear.setVisible(true);
    }

    public void ventanaCrearApunte(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialCrearApunte materialCrearApunte = new MaterialCrearApunte(mapaArticulos);
        materialCrearApunte.setVisible(true);
    }

    public void ventanaCrearCd(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialCrearCD a = new MaterialCrearCD(mapaArticulos);
        a.setVisible(true);
    }

    public void ventanaCrearLibro(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialCrearLibro a = new MaterialCrearLibro(mapaArticulos);
        a.setVisible(true);
    }

    public void ventanaCrearRevista(LinkedHashMap<Integer, Articulo> mapaArticulos) {
        MaterialCrearRevista a = new MaterialCrearRevista(mapaArticulos);
        a.setVisible(true);
    }
}
