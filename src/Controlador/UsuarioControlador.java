/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Usuario;
import Vista.UsuarioCrear;
import Vista.UsuarioEliminar;
import Vista.UsuarioLeer;
import Vista.UsuarioMain;
import Vista.UsuarioModificar;
import hirondelle.date4j.DateTime;
import java.util.LinkedHashMap;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author tincho
 */
public class UsuarioControlador {

    private void limpiarUsuarios(JTable tablaUsuarios) {
        if (tablaUsuarios.isValid()) {
            DefaultTableModel temp = (DefaultTableModel) tablaUsuarios.getModel();
            for (; temp.getRowCount() > 0;) {
                temp.removeRow(0);
            }
        }
    }

    public void leerUsuarios(LinkedHashMap<Integer, Usuario> mapaUsuarios, JTable jTableUsuarios) {
        if (jTableUsuarios.isValid()) {
            limpiarUsuarios(jTableUsuarios);
            for (Integer i : mapaUsuarios.keySet()) {
                DefaultTableModel temp = (DefaultTableModel) jTableUsuarios.getModel();
                Usuario u = mapaUsuarios.get(i);
                String[] fechaNacimiento = u.getFechaNacimiento().toString().split("-");
                String nacimiento = fechaNacimiento[2] + "/" + fechaNacimiento[1] + "/" + fechaNacimiento[0];
                Object[] fila = {u.getDni(), u.getApellido(), u.getNombre(), nacimiento};
                temp.addRow(fila);
            }
        }

    }

    public void ventanaLeer(LinkedHashMap<Integer, Usuario> mapaUsuarios) {
        if (mapaUsuarios.size() > 0) {
            UsuarioLeer usuarioLeer = new UsuarioLeer(mapaUsuarios);
            usuarioLeer.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "No hay usuarios cargados, por lo que no se pueden leer usuarios.");
        }
    }

    public void ventanaMain(LinkedHashMap<Integer, Usuario> mapaUsuarios) {
        UsuarioMain usuarioMain = new UsuarioMain(mapaUsuarios);
        usuarioMain.setVisible(true);
    }

    public void eliminarUsuarios(LinkedHashMap<Integer, Usuario> mapaUsuarios, JTable jTableUsuarios) {
        if (jTableUsuarios.isValid()) {
            int[] seleccionados = jTableUsuarios.getSelectedRows();
            if (seleccionados.length > 0) {
                DefaultTableModel temp = (DefaultTableModel) jTableUsuarios.getModel();
                for (int i : seleccionados) {
                    int indice = (int) temp.getValueAt(i, 0);
                    mapaUsuarios.remove(indice);
                }
                leerUsuarios(mapaUsuarios, jTableUsuarios);
                JOptionPane.showMessageDialog(null, "Usuarios eliminados exitosamente.");

            } else {
                JOptionPane.showMessageDialog(null, "Seleccione usuarios para eliminar.");
            }

        }
    }

    public void ventanaEliminar(LinkedHashMap<Integer, Usuario> mapaUsuarios) {
        if (mapaUsuarios.size() > 0) {
            UsuarioEliminar usuarioEliminar = new UsuarioEliminar(mapaUsuarios);
            usuarioEliminar.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "No se pueden eliminar usuarios porque no hay usuarios cargados.");
        }
    }

    public void ventanaModificar(LinkedHashMap<Integer, Usuario> mapaUsuarios) {
        if (mapaUsuarios.size() > 0) {
            UsuarioModificar usuarioModificar = new UsuarioModificar(mapaUsuarios);
            usuarioModificar.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(null, "No se pueden eliminar usuarios porque no hay usuarios cargados.");
        }
    }

    public void seleccionarUsuario(JTable jTableUsuarios, JTextField dni, JTextField apellido, JTextField nombre, JTextField dia, JTextField mes, JTextField year) {
        if (jTableUsuarios.isValid() && dni.isValid() && apellido.isValid() && nombre.isValid() && dia.isValid() && mes.isValid() && year.isValid()) {
            Integer seleccionado = jTableUsuarios.getSelectedRow();
            if (seleccionado >= 0) {
                DefaultTableModel temp = (DefaultTableModel) jTableUsuarios.getModel();
                String documento = temp.getValueAt(seleccionado, 0).toString();
                String ap = temp.getValueAt(seleccionado, 1).toString();
                String nom = temp.getValueAt(seleccionado, 2).toString();
                String fecha = temp.getValueAt(seleccionado, 3).toString();
                String[] array = fecha.split("/");
                dia.setText(array[0]);
                mes.setText(array[1]);
                year.setText(array[2]);
                dni.setText(documento);
                apellido.setText(ap);
                nombre.setText(nom);
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un usuario.");
            }
        }

    }

    public void modificarUsuario(LinkedHashMap<Integer, Usuario> mapaUsuarios, JTable jTableUsuarios, JTextField jTextFieldDNI, JTextField jTextFieldApellido, JTextField jTextFieldNombre, JTextField jTextFieldDia, JTextField jTextFieldMes, JTextField jTextFieldY) {
        if (jTableUsuarios.isValid() && jTextFieldDNI.isValid() && jTextFieldApellido.isValid() && jTextFieldNombre.isValid() && jTextFieldDia.isValid() && jTextFieldMes.isValid() && jTextFieldY.isValid()) {
            Boolean dniValido = Vistas.getInstance().articulo.validarInput(jTextFieldDNI.getText());
            StringBuilder st = new StringBuilder();
            st.append(jTextFieldY.getText());
            st.append("-");
            st.append(jTextFieldMes.getText());
            st.append("-");
            st.append(jTextFieldDia.getText());
            JTextField fecha = new JTextField(st.toString());
            Boolean nacimientoValido = Vistas.getInstance().alquiler.validarFecha(fecha);
            if (dniValido && nacimientoValido) {
                Usuario u = mapaUsuarios.get(Integer.parseInt(jTextFieldDNI.getText()));
                u.setApellido(jTextFieldApellido.getText());
                u.setNombre(jTextFieldNombre.getText());
                u.setFechaNacimiento(new DateTime(st.toString()));
                leerUsuarios(mapaUsuarios, jTableUsuarios);
                JOptionPane.showMessageDialog(null, "Usuario modificado exitosamente.");
            } else {
                StringBuilder builder = new StringBuilder();
                builder.append("Datos invalidos:");
                if (!dniValido) {
                    builder.append("\nDNI invalido.");
                }
                if (!nacimientoValido) {
                    builder.append("\nFecha de nacimiento invalida.");
                }
                JOptionPane.showMessageDialog(null, builder.toString());
            }
        }
    }

    public void crearUsuario(LinkedHashMap<Integer, Usuario> usuarios, JTextField dni, JTextField apellido, JTextField nombre, JTextField dia, JTextField mes, JTextField year) {
        if (dni.isValid() && apellido.isValid() && nombre.isValid() && dia.isValid() && mes.isValid() && year.isValid()) {
            Boolean dniValido = Vistas.getInstance().articulo.validarInput(dni.getText());
            Boolean noRepetido = false;
            if (dniValido) {
                noRepetido = !usuarios.containsKey(Integer.parseInt(dni.getText()));
            }
            StringBuilder st = new StringBuilder();
            st.append(year.getText());
            st.append("-");
            st.append(mes.getText());
            st.append("-");
            st.append(dia.getText());
            JTextField fecha = new JTextField(st.toString());
            Boolean nacimientoValido = Vistas.getInstance().alquiler.validarFecha(fecha);
            if (dniValido && nacimientoValido && noRepetido) {
                Usuario u = new Usuario(apellido.getText(), nombre.getText(), Integer.parseInt(dni.getText()), new DateTime(st.toString()));
                usuarios.put(u.getDni(), u);
                JOptionPane.showMessageDialog(null, "Usuario agregado exitosamente.");
            } else {
                StringBuilder builder = new StringBuilder();
                builder.append("Datos invalidos:");
                if (!dniValido) {
                    builder.append("\nDNI invalido.");
                } else {
                    if (!noRepetido) {
                        builder.append("\nEl DNI ya existe.");
                    }
                }
                if (!nacimientoValido) {
                    builder.append("\nFecha de nacimiento invalida.");
                }
                JOptionPane.showMessageDialog(null, builder.toString());
            }
        }
    }

    public void ventanaCrear(LinkedHashMap<Integer, Usuario> mapaUsuarios) {
        UsuarioCrear usuarioCrear = new UsuarioCrear(mapaUsuarios);
        usuarioCrear.setVisible(true);
    }
}
