/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

/**
 *
 * @author tincho
 */
public final class Vistas {
    
    public final ArticuloControlador articulo;
    public final AlquilerControlador alquiler;
    public final UsuarioControlador usuario;
    
    private Vistas() {
        this.usuario = new UsuarioControlador();
        this.alquiler = new AlquilerControlador();
        this.articulo = new ArticuloControlador();
    }
    
    public static Vistas getInstance() {
        return VistasControladorHolder.INSTANCE;
    }
    
    private static class VistasControladorHolder {

        private static final Vistas INSTANCE = new Vistas();
    }
}
