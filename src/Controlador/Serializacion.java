/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;

/**
 *
 * @author tincho
 */
public abstract class Serializacion<K,V> {

    /**
     *
     *
     * @param lista
     */
    public void serializar(LinkedHashMap<K,V> lista) {
        String nombre;
        if (this.getClass().getSimpleName().equalsIgnoreCase("Usuario")) {
            nombre = "usuarios.ser";
        } else {
            nombre = "articulos.ser";
        }
        try {
            FileOutputStream archivo = new FileOutputStream(nombre);
            BufferedOutputStream buffer = new BufferedOutputStream(archivo);
            ObjectOutputStream objeto = new ObjectOutputStream(buffer);
            try {
                objeto.writeObject(lista);
            } finally {
                objeto.close();
            }
        } catch (IOException e) {
        }
    }

    ;
    
    /**
     *
     * @return
     */
    public LinkedHashMap<K,V> deserializar() {
        LinkedHashMap<K,V> mapa = new LinkedHashMap<>();
        String nombre;
        if (this.getClass().getSimpleName().equalsIgnoreCase("Usuario")) {
            nombre = "usuarios.ser";
        } else {
            nombre = "articulos.ser";
        }
        try {
            FileInputStream archivo = new FileInputStream(nombre);
            BufferedInputStream buffer = new BufferedInputStream(archivo);
            ObjectInputStream objeto = new ObjectInputStream(buffer);
            try {
                mapa = (LinkedHashMap<K,V>) objeto.readObject();
            } finally {
                objeto.close();
            }
        } catch (IOException | ClassNotFoundException ex) {
        }
        return mapa;
    }
;
}
